<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = \App\User::paginate(10); 

      return view('users/index', compact('users')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('users/create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       if($request->get('password') != $request->get('password_confirmation')){

          session()->flash('message', 'GAGAL: Password dan konfirmasi password harus sama!');
          
          return redirect()->to('users/create')->withInput($request->except(['password', 'password_confirmation']));
        
       }


       $payload = $request->except('_token');

       $payload['password'] = \Hash::make($request->get('password'));

       \App\User::create($payload); 

       session()->flash('message', 'User berhasil disimpan');

       return redirect()->to('users/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user = \App\User::findOrFail($id); 

      return view('users/create', compact('user')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $user = \App\User::findOrFail($id); 

      return view('users/edit', compact('user')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $payload = $request->except(['_token', '_method']);

      \App\User::findOrFail($id)->update($payload); 

      session()->flash('message', 'User berhasil diupdate'); 

      return redirect()->to("users/$id/edit");
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      \App\User::findOrFail($id)->delete(); 

       return redirect()->to('users'); 
    }
}
