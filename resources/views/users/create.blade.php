
@extends('layouts.app')

@section('content') 

  <div class="container">

    <form method="POST" action={{action("UserController@store")}}>

      {{csrf_field()}}

      <div class="form-group">
        @if(session()->has('message'))

          <div class="alert alert-info">
            {{ session('message')}}
          </div>
        @endif
      </div>

      <div class="form-group">
        <h3> Tambah user </h3>
      </div>

      <div class="form-group">
        <label for="name" > Name </label>
        <input value="{{Request::old('name')}}" type="text" class="form-control" id="name" name="name" />
      </div>

      <div class="form-group">
        <label for="email" > Email </label>
        <input value="{{Request::old('email')}}" type="text" class="form-control" id="email" name="email" />
      </div>

      <div class="form-group">
        <label for="password" > password </label>
        <input type="password" class="form-control" id="password" name="password" />
      </div>

      <div class="form-group">
        <label for="password_confirmation" > konfirmasi password </label>
        <input type="password" class="form-control" id="password" name="password_confirmation" />
      </div>
      
      <div class="form-group">
        <input type="submit" class="btn btn-primary" />
      </div>
    </form>
  </div>
@endsection
