
@extends('layouts.app') 

@section('content') 

  <div class="container"> 

    <div class="row mb-2">
      <div class="col-md-12">
        <h3> Daftar user </h3>
        <br/>
        <a class="btn btn-primary" href="{{action('UserController@create')}}"> Tambah </a>
      </div>
    </div>

    <table class="table table-bordered">
      <thead>
        <tr>
          <th> Name </th>
          <th> Action </th>
        </tr>
      </thead>
      <tbody>
       @foreach($users as $user)
        <tr> 
          <td> {{$user->name}} </td>
          <td>
            <a href="{{action('UserController@edit', ['id' => $user->id])}}" class="btn btn-success btn-sm"> edit </a> 

            <form 
              action="{{action('UserController@destroy', ['id' => $user->id])}}" 
              method="POST"
              class="d-inline"
             >
              <input type="hidden" name="_method" value="DELETE" />
              {{csrf_field()}}
              <input type="submit" class="btn btn-danger btn-sm" value="delete">
            </form>
          </td>
        </tr>
       @endforeach

       <tfoot>
         <tr>
           <td colspan="4">
            {{$users->links()}}
           </td>
         </tr>
       </tfoot>
      </tbody>
    </table>
  </div>
@endsection
