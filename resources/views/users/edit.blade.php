

@extends('layouts.app')

@section('content') 

  <div class="container">

    <form method="POST" action={{action("UserController@update", ['id' => $user->id])}}>

      <input type="hidden" name="_method" value="PUT" />

      {{csrf_field()}}

      <div class="form-group">
        @if(session()->has('message'))

          <div class="alert alert-info">
            {{ session('message')}}
          </div>
        @endif
      </div>

      <div class="form-group">
        <h3> Edit user </h3>
      </div>

      <div class="form-group">
        <label for="name" > Name </label>
        <input value="{{$user->name}}" type="text" class="form-control" id="name" name="name" />
      </div>

      <div class="form-group">
        <input type="submit" class="btn btn-primary" />
      </div>
    </form>
  </div>
@endsection
