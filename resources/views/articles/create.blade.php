
@extends('layouts.app')

@section('content') 

  <div class="container">

    <form method="POST" action={{action("ArticleController@store")}}>

      {{csrf_field()}}

      <div class="form-group">
        @if(session()->has('message'))

          <div class="alert alert-info">
            {{ session('message')}}
          </div>
        @endif
      </div>

      <div class="form-group">
        <h3> Tambah artikel </h3>
      </div>

      <div class="form-group">
        <label for="title" > Judul </label>
        <input type="text" class="form-control" id="title" name="title" />
      </div>
      
      <div class="form-group">
        <label for="content"> Konten </label> 
        <textarea class="form-control" id="content" name="content"></textarea>
      </div>

      <input type="hidden" name="creator_id" value="{{\Auth::user()->id}}">

      <div class="form-group">
        <input type="submit" class="btn btn-primary" />
      </div>
    </form>
  </div>
@endsection
