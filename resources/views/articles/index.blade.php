@extends('layouts.app') 

@section('content') 

  <div class="container"> 

    <div class="row mb-2">
      <div class="col-md-12">
        <h3> Daftar artikel </h3>
        <br/>
        <a class="btn btn-primary" href="{{action('ArticleController@create')}}"> Tambah </a>
      </div>
    </div>

    <table class="table table-bordered">
      <thead>
        <tr>
          <th> Judul </th>
          <th> Penulis </th>
          <th> Action </th>
        </tr>
      </thead>
      <tbody>
       @foreach($articles as $article)
        <tr> 
          <td> {{$article->title}} </td>
          <td> {{$article->author->name}} </td>
          <td>
            <a href="{{action('ArticleController@edit', ['id' => $article->id])}}" class="btn btn-success btn-sm"> edit </a> 

            <form 
              action="{{action('ArticleController@destroy', ['id' => $article->id])}}" 
              method="POST"
              class="d-inline"
             >
              <input type="hidden" name="_method" value="DELETE" />
              {{csrf_field()}}
              <input type="submit" class="btn btn-danger btn-sm" value="delete">
            </form>
          </td>
        </tr>
       @endforeach

       <tfoot>
         <tr>
           <td colspan="4">
            {{$articles->links()}}
           </td>
         </tr>
       </tfoot>
      </tbody>
    </table>
  </div>
@endsection
