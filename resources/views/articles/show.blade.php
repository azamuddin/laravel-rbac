@extends('layouts.app') 

@section('content') 

  <div class='container'>
    <div class="row">
       
      <div class="col-md-8 mx-auto">

          <h3> {{$article->title}} </h3>

          <p> 
             {{$article->content}}
          </p>
      </div>

    </div>
  </div>
@endsection
